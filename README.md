# My RISC-V Core Processor



## Getting started

A Simplified RISC-V processor implemented to for a few instructions. 

## Authors and acknowledgment

Author: Zille Huma Kamal 

Acknowledgment: Thanks to the work in RISC-V Core Processor by [Steve Hoover](https://github.com/stevehoover) 
